'use strict';

var gulp = require('gulp');
var browserSync = require('browser-sync');
var nodemon = require('gulp-nodemon');
var $ = require('gulp-load-plugins')();

var sassPaths = [
  'node-modules/normalize.scss/sass',
  'node-modules/foundation-sites/scss',
  'node-modules/motion-ui/src'
];

gulp.task('sass', function () {
    return gulp.src('scss/app.scss')
        .pipe($.sass({
                includePaths: sassPaths,
                outputStyle: 'compressed' // if css compressed **file size**
            })
            .on('error', $.sass.logError))
        .pipe($.autoprefixer({
            browsers: ['last 2 versions', 'ie >= 9']
        }))
        .pipe(gulp.dest('public/stylesheets'));
});

gulp.task('browser-sync', ['nodemon'], function () {
    browserSync.init(null, {
        proxy: "http://localhost:5000",
        files: ["public/**/*.*", "views/*.pug"],
        browser: "google chrome",
        port: 7000,
    });
});

gulp.task('nodemon', function (cb) {

    var started = false;

    return nodemon({
        script: 'app.js'
    }).on('start', function () {
        // to avoid nodemon being started multiple times
        // thanks @matthisk
        if (!started) {
            cb();
            started = true;
        }
    });
});

gulp.task('default', ['sass', 'browser-sync'], function () {
    gulp.watch(['scss/**/*.scss'], ['sass']);
});
